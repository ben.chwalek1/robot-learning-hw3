import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm  # norm.pdf can be used to calculate kernel function values

# Seed for reproducibility
np.random.seed(23)

# Number of Samples
N = 10

# Number of evaluation points
E = 100


####################### Part 1 #######################
# Generate domain data to fit using kernel regression
start_x = -2*np.pi
end_x = 2*np.pi
x_N_1 = np.linspace(start_x, end_x, N).reshape(N,1)

# Define true function
f_y = lambda x : np.sin(x) # True function (without the outliers)

# Make y output noisy
noise_scale = 0.3
y_N_1 = f_y(x_N_1) + noise_scale*np.random.randn(N,1)



####################### Part 2 (4 points) #######################
# Define the number of basis functions
K = 10

# Create Kernel Matrix Phi_N_K (1 point)
means = np.linspace(start_x, end_x, K)
means, x = np.meshgrid(means, x_N_1)
Phi_N_K = norm.pdf(x - means)
# (FOR PART ONE) Solve for weights w_K_1 (1 points)
w_K_1 = np.linalg.inv(Phi_N_K.T@Phi_N_K)@Phi_N_K.T @ y_N_1
# (FOR PART TWO) Solve for weights w_K_1 by adding ridge penality (2 points)
# lamb = 1e-3 * N
# w_K_1 = np.linalg.inv(Phi_N_K.T@Phi_N_K + lamb*np.eye(K))@Phi_N_K.T @ y_N_1


####################### Part 3 (2 points) #######################
### Predict on evaluation domain
# d_E_1 = # create linearly spaced domain with E samples
x_E_1 = np.linspace(start_x, end_x, E).reshape(E, 1)
# Phi_E_K = # create Phi_E_K, evaluating pdf on domain d_E_1 (1 point)
means = np.linspace(start_x, end_x, K)
means, x = np.meshgrid(means, x_E_1)
Phi_E_K = norm.pdf(x - means)
# yhat_E_1 = # multiply Phi_E_K with w_K_1 (1 point)
yhat_E_1 = Phi_E_K @ w_K_1


####################### Part 4 (1 point) #######################
# Plot the original data and the kernel regression estimate 
# Credit is given for correctly plotting the non-regularized kernel regression output and attaching the figure to your homework (1 point)
def plot_results(x_E_1, yhat_E_1):
    """
    NOTE: x and y are E x 1 vectors, where E represents the number of points in the evaluation domain
    - x_E_1: evaluation domain (should be linearly spaced)
    - yhat_E_1: regression output
    """
    plt.figure(figsize=(10,6), layout='tight')
    plt.scatter(x_N_1[:,0], y_N_1[:,0], color='black', alpha=0.8, label='Original Samples')
    plt.plot(x_E_1[:,0], yhat_E_1[:,0], color='red', lw=3, label='Regression Prediction')
    plt.plot(x_E_1[:,0], f_y(x_E_1)[:,0], color='black', lw=3, linestyle='--', label='y True')

    # for i in range(Phi_N_K.shape[1]):
    #     plt.plot(x_N_1[:,0], Phi_N_K[:,i])
    
    plt.legend()
    plt.title("Kernel Regression")
    plt.show()


plot_results(x_E_1, yhat_E_1)
