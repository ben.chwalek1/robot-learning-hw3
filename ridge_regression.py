import numpy as np
import matplotlib.pyplot as plt

#lololololol

# Seed for reproducibility
np.random.seed(23)

# Number of Samples
N = 100
# Number of evaluation points
E = 100


####################### Part 1 #######################
# Generate data to fit using ridge regression

# Randomly sample x locations
x_N_1 = np.random.rand(N, 1)

# Define true function
f_y = lambda x : 3 * x 

# Make y output noisy
noise_scale = 0.3
y_N_1 = f_y(x_N_1) + noise_scale*np.random.randn(N, 1)*2

# Sort x and y data
sorted_idxs = np.argsort(y_N_1, axis=0).flatten()
y_N_1 = y_N_1[sorted_idxs,:]
x_N_1 = x_N_1[sorted_idxs,:]

# Add some out of distribution data
x_offset = 0.5
x_N_1[:3] -= x_offset
x_N_1[-3:] += x_offset

y_offset = 5
y_N_1[:3] -= y_offset
y_N_1[-3:] += y_offset

####################### Part 2 (5 points) #######################
# Perform gradient descent updates

### Ridge regression parameters
learning_rate = .01
num_epochs = 1000
lamb = 5 # regularization strength (try different values)

# X_N_2 = # prepend bias/ones column to x_N_1 (instead of using intercept)
X_N_2 = np.hstack((np.ones((N,1)), x_N_1))
# w_2_1 = # Initialize the weights
w_2_1 = np.ones((2,1))

### Ridge regression training 
# Run several epochs, updating w_2_1 every iteration with the ridge gradient and weight gradient 
# (3 points for correct weight gradient updates, 2 points for correct ridge gradient updates)
# TODO: YOUR CODE HERE
for i in range(num_epochs):
    grad = -((y_N_1 - X_N_2@w_2_1).T @ X_N_2).T + lamb*w_2_1
    w_2_1 = w_2_1 - learning_rate*grad

# lambda high -> keep weights low and vice versa
# ergo the function stays flatter and vice versa
# lambda too high -> oscillations
print(w_2_1)

####################### Part 3 (1 point) #######################
### Generate test data points (fill in the correct code below)
# x_E_1 = # create a linearly-spaced interval with E samples
x_E_1 = np.linspace(0, 1, N).reshape(E,1)
# X_E_2 = # prepend bias/ones column to x_E_1
X_E_2 = np.hstack((np.ones((E, 1)), x_E_1))


### Predict the target values for the test data (1 point)
# yhat_E_1 = # multiply X_E_2  with  w_2_1
yhat_E_1 = X_E_2 @ w_2_1


####################### Part 4 (1 point) #######################
### Plot the original data and the ridge regression line
# Credit is given for correctly plotting the ridge regression output and attaching the figure to your homework (1 point)
# TODO: YOUR CODE HERE
def plot_results(x_E_1, yhat_E_1, lamb):
    plt.figure(figsize=(10,6), layout='tight')
    plt.scatter(x_N_1[:,0], y_N_1[:,0], color='black', alpha=0.8, label='Original Samples')
    plt.plot(x_E_1[:,0], yhat_E_1[:,0], color='red', lw=3, label=f'Ridge Regression Prediction (λ={lamb})')
    plt.plot(x_E_1[:,0], f_y(x_E_1)[:,0], color='blue', linestyle='--', lw=3, label='y True')
    
    plt.legend()
    plt.title('Ridge Regression')
    plt.show()


# TODO: Plot your results:
plot_results(x_E_1, yhat_E_1, lamb)
