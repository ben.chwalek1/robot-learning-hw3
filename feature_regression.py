import numpy as np
import matplotlib.pyplot as plt

# Seed for reproducibility
np.random.seed(23)

# Number of Samples
N = 10
# Number of evaluation points
E = 100

# NOTE: A variable of the form "A_B_C" means: Name = A, Dimension 1 = B, Dimension 2 = C
# Example: X_N_2 means an X matrix of shape N by 2


####################### Part 1 #######################
# Generate synthetic data to fit for regression: arccos(x) - shift
shift = 5
ymin = -3
ymax = 3
lim = 1

# Define the x domain
x_N_1 = np.linspace(-lim, lim, N).reshape(N,1)

# Define xp_N_1 = "x prime", the modified x values with trended noise
noise_scale = 0.1
epsilon = np.linspace(0, 0.3, N).reshape(N,1) + noise_scale*np.random.randn(N,1)
xp_N_1 = np.arccos(np.clip(x_N_1 +  epsilon, -1, 1))  - shift

# Define true function
f_y = lambda x_T_1 : np.linspace(-lim, lim, x_T_1.shape[0]).reshape(x_T_1.shape[0],1)

# Define ground truth labels (evaluated on noisy x prime values)
y_N_1 = f_y(xp_N_1)


####################### Part 2 (2 points) #######################
### Apply feature transformaton of features: cos(x + shift)   (2 points)
xf_N_1 = np.cos(xp_N_1 + shift)


####################### Part 3 (2 points) #######################
# Fit regression in the transformed space
# Xf_N_2 = # prepend bias/ones column to x_N_1 (instead of using intercept)
Xf_N_2 = np.hstack((np.ones((N, 1)), xf_N_1))

# Solve for weights
# w_2_1 = # TODO: solve for the weights here (2 points)
w_2_1 = np.linalg.inv(Xf_N_2.T@Xf_N_2)@Xf_N_2.T@y_N_1


####################### Part 4 (2 points) #######################
### Generate test data points (fill in the correct code below)
# x_E_1 = # create a linearly-spaced interval with E samples on original domain
x_E_1 = np.linspace(-lim, lim, E).reshape(E, 1)
# X_E_2 = # prepend bias/ones column to x_E_1
X_E_2 = np.hstack((np.ones((E,1)), x_E_1))

### Predict the target values for the test data
# yhat_E_1 = # multiply X_E_2  with  w_2_1 (2 points)
yhat_E_1 = X_E_2 @ w_2_1


####################### Part 5 (2 points) #######################
# Credit is given for correctly plotting the regression (in the transformed space, as well as the inverse-transformed space) and attaching the figure to your homework (2 points)
def plot_results(x_E_1, yhat_E_1, xf_N_1):
    plt.figure(figsize=(10,6), layout='tight')
    plt.plot(x_E_1[:,0], yhat_E_1[:,0], color='blue', lw=3, label=r'$\hat{y} = x\hat{w}$')
    plt.scatter(xf_N_1[:,0], y_N_1[:,0], color='orange', alpha=0.8, label=r'$y_i | x^f_i$')

    plt.plot(np.arccos(x_E_1)[:,0] - shift, yhat_E_1[:,0], color='red', lw=3, label=r'$\hat{y} = x^\prime \hat{w} $')
    plt.plot(np.arccos(x_E_1)[:,0] - shift, f_y(x_E_1), color='black', lw=3, linestyle='--', label=r'$y | x^\prime$')
    plt.scatter(xp_N_1[:,0], y_N_1[:,0], color='black', alpha=0.8, label=r'$y_i | (x_i^\prime + \varepsilon)$')

    plt.legend()
    plt.title("Feature Transformation Regression")
    plt.show()


# TODO: Plot your results:
plot_results(x_E_1, yhat_E_1, xf_N_1)
