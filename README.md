**Install the requirements.txt file**
Simply install requirements.txt using your preferred method (e.g, pip, venv, or conda)

**Run the python scripts**
  1. For Problem 3.2, run: 
  
    python feature_regression.py
  
  2. For Problem 3.3, run:
  
    python ridge_regression.py
  
  3. For Problem 3.4, run:
  
    python kernel_regression.py
  
  4. For Problem 3.5, run:

    python knn.py

**How to use the exercise latex template:**
  1. You find the template as *.zip file in the homework directory (keep it as zip):

    exercise_template.zip

  2. Open your overleaf:

    https://sharelatex.tu-darmstadt.de/

  3. Choose the template:

    new project >  upload project > select a zip file 

  4. Now you are able to compile it directly

**Additional Information:**
  * Submit your code as a python file for every task beside the PDF file.
