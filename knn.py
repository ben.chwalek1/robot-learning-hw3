import numpy as np
import matplotlib.pyplot as plt


####################### Part 1 #######################
# Load the train and test data
knn_data = np.load('knn_data.npz')
X_train = knn_data['X_train']
y_train = knn_data['y_train']

X_test = knn_data['X_test']
y_test = knn_data['y_test']



####################### Part 2 (3 points) #######################
# Compute the accuracy on the test dataset using k=3 nearest neighbors (3 points)

def predict(X_train, y_train, X_test, k=3):
    return [np.round(np.mean(y_train[np.argsort(np.linalg.norm(x - X_train, axis=-1))[:k]])) for x in X_test]

preds = predict(X_train, y_train, X_test)
print(f"accuracy for k=3: {np.sum(preds == y_test) / len(preds)}")

####################### Part 2 (2 points) #######################
# Compute the 5-fold accuracy, using just the test set as the full dataset with all values of k from k=1 to k=15 (2 points)
# TODO: YOUR CODE HERE

# use np methods instead of for loop
# cluster neighbouring points in training set
# dimensionality reduction
# spatial indexing (build data structure for faster distance lookups)

# def train_val_split(x, y, j, k=5):
#     xs, ys = np.split(x, k), np.split(y, k)
#     xs, ys = np.array(xs), np.array(ys)
#     mask = np.arange(k) == j
#     train_x = np.concatenate(xs[np.arange(k)[np.logical_not(mask)]])
#     train_y = np.concatenate(ys[np.arange(k)[np.logical_not(mask)]])
#     val_x = np.concatenate(xs[np.arange(k)[mask]])
#     val_y = np.concatenate(ys[np.arange(k)[mask]])
#
#     return train_x, train_y, val_x, val_y


def train_val_split(x, y, j, k=5):
    n = len(x)
    indices = np.arange(n)
    fold_size = n // k

    val_indices = indices[j * fold_size: (j + 1) * fold_size]
    train_indices = np.delete(indices, val_indices)

    return x[train_indices], y[train_indices], x[val_indices], y[val_indices]

    # train_x, val_x = x[train_indices], x[val_indices]
    # train_y, val_y = y[train_indices], y[val_indices]



val_k = 5
ks = 15
acc = [0]*ks
for k in range(1, ks+1):
    for v in range(val_k):
        train_x, train_y, val_x, val_y = train_val_split(X_train, y_train, v)

        preds = predict(train_x, train_y, val_x, k)
        acc[k-1] += np.sum(preds == val_y) / len(preds) / val_k

# acc = [.5] * 15 + np.random.rand(15) / 2

best_k = np.argmax(acc) + 1


####################### Part 3 (2 points) #######################
# Display results to see which k value gives the best accuracy using the 5-fold model selection (2 points)
plt.xticks(range(1, len(acc)+1))
plt.xlabel("k")
# plt.yticks(np.arange(0, 1.1, step=.1))
plt.ylabel("accuracy")
plt.axvline(x=best_k, color='red')
plt.plot(range(1, len(acc)+1), acc)
plt.ylim(top=1)
plt.show()

# uneven parameters seem to do better, because we do not introduce a bias when the classes of the neighbours are evenly split
# TODO: YOUR CODE HERE
